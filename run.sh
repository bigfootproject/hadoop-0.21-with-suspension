#!/bin/bash

if [ $# -ne 2 ]; then
  echo "Usage: ${0} (dfs|mr) (start|stop)"
  exit 0
fi

if [ \( ${1} != "dfs" -a ${1} != "mr" \) -o \( ${2} != "start" -a ${2} != "stop" \) ]; then
  echo "Usage: ${0} (dfs|mr) (start|stop)"
  exit 1
fi

DIR=`dirname ${0}`
cd ${DIR}
ADIR=`pwd`
CMD=${1}
ACTION=${2}
export HADOOP_COMMON_HOME="${ADIR}/common"
export HADOOP_HDFS_HOME="${ADIR}/hdfs"
export HADOOP_MAPRED_HOME="${ADIR}/mapreduce"
export JAVA_HOME="/usr/lib/jvm/java-6-oracle/jre/"

shift;shift;

# copy configurations
if ! diff "${ADIR}/common_conf/core-site.xml" \
	  "${HADOOP_COMMON_HOME}/conf/core-site.xml"; then
  rm -r "${HADOOP_COMMON_HOME}/conf"
  cp -r "${ADIR}/common_conf" "${HADOOP_COMMON_HOME}/conf"
fi
if ! diff "${ADIR}/hdfs_conf/hdfs-site.xml" \
          "${HADOOP_HDFS_HOME}/conf/hdfs-site.xml"; then
  rm -r "${HADOOP_HDFS_HOME}/conf"
  cp -r "${ADIR}/hdfs_conf" "${HADOOP_HDFS_HOME}/conf"
fi
if ! diff "${ADIR}/mapred_conf/mapred-site.xml" \
          "${HADOOP_MAPRED_HOME}/conf/mapred-site.xml"; then
  rm -r "${HADOOP_MAPRED_HOME}/conf"
  cp -r "${ADIR}/mapred_conf" "${HADOOP_MAPRED_HOME}/conf"
fi

if   [ ${CMD} = "dfs" -a ${ACTION} = "start" ]; then
  cd ${HADOOP_HDFS_HOME}
  ./bin/start-dfs.sh &
elif [ ${CMD} = "dfs" -a ${ACTION} = "stop" ]; then
  cd ${HADOOP_HDFS_HOME}
  ./bin/stop-dfs.sh &
elif [ ${CMD} = "mr"  -a ${ACTION} = "start" ]; then
  cd ${HADOOP_MAPRED_HOME}
  ./bin/start-mapred.sh &
elif [ ${CMD} = "mr"  -a ${ACTION} = "stop" ]; then
  cd ${HADOOP_MAPRED_HOME}
  ./bin/stop-mapred.sh &
fi
