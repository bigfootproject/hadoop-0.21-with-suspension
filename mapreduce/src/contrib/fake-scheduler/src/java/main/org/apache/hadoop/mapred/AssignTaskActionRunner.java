package org.apache.hadoop.mapred;

import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.apache.hadoop.mapreduce.server.jobtracker.TaskTracker;


public class AssignTaskActionRunner extends ActionRunner {

  private final JobInProgress jip;
  private final boolean isMap;
  private Task assigned;

  public AssignTaskActionRunner(JobInProgress jip, boolean isMap) {
    if (jip == null)
      throw new NullPointerException();
    this.jip = jip;
    this.isMap = isMap;
  }

  public Task getAssigned() {
    return this.assigned;
  }

  @Override
  public void run( JobTracker jobTracker
                 , TaskTracker taskTracker
                 , Collection<JobInProgress> jobs) throws IOException {
    this.assigned = this.obtainNewTask( this.jip
                                      , this.isMap
                                      , jobTracker
                                      , taskTracker.getStatus());
  }

  private Task obtainNewTask( JobInProgress jip
                            , boolean isMap
                            , JobTracker jobTracker
                            , TaskTrackerStatus status) throws IOException {
    int numUniqueHosts = jobTracker.getNumberOfUniqueHosts();
    ClusterStatus clusterStatus = jobTracker.getClusterStatus();

    return isMap ? jip.obtainNewMapTask( status
                                       , clusterStatus.getMaxMapTasks()
                                       , numUniqueHosts
                                       , jip.anyCacheLevel)
                 : jip.obtainNewReduceTask( status
                                          , clusterStatus.getMaxReduceTasks()
                                          , numUniqueHosts);
  }

  @Override
  public String toString() {
    return "assign" + (isMap ? "Map" : "Reduce") 
                    + "(" + jip.getJobID().toString() + ")";
  }

}
