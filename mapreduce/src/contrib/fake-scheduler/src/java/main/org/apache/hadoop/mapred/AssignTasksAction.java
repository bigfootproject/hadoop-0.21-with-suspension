package org.apache.hadoop.mapred;

import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.apache.hadoop.mapreduce.server.jobtracker.TaskTracker;


public class AssignTasksAction extends Action {

  protected List<Task> assigned;

  public AssignTasksAction( PreconditionsChecker checker
                          , AssignTaskActionRunner runner) {
    super(checker, runner);
    this.assigned = new LinkedList<Task>();
  }

  @Override
  public boolean checkAndRun( JobTracker jobTracker
                            , TaskTracker taskTracker
                            , Collection<JobInProgress> jobs)
      throws IOException {
    boolean result = super.checkAndRun(jobTracker, taskTracker, jobs);
    if (result) {
      Task task = ((AssignTaskActionRunner) this.runner).getAssigned();
      if (task != null)
        this.assigned.add(task);
    }
    return result;
  }

  public List<Task> getAssigned() {
    return this.assigned;
  }
}
