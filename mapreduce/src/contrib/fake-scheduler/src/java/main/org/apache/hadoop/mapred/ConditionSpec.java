package org.apache.hadoop.mapred;

import java.util.Map;

public class ConditionSpec {

  private final String jobSpecID;
  private final ConditionInternal condition;

  private ConditionSpec(String jobSpecID, ConditionInternal condition) {
    this.jobSpecID = jobSpecID;
    this.condition = condition;
  }

  // a condition is written <jobid>.<condition>
  public static ConditionSpec fromStringOrNull(String str) {
    String[] chunks = str.trim().split("\\.",2);
    if (chunks.length != 2)
      return null;

    if ("inited".equals(chunks[1]))
      return new ConditionSpec(chunks[0],new JobInitedInt());
    else if ("completed".equals(chunks[1]))
      return new ConditionSpec(chunks[0],new JobCompletedInt());
    else if ("scheduleReduces".equals(chunks[1]))
      return new ConditionSpec(chunks[0],new JobScheduleReducesInt());
    else if ("hasMapSuspended".equals(chunks[1]) ||
             "hasMapsSuspended".equals(chunks[1]))
      return new ConditionSpec(chunks[0],new JobHasTaskSuspendedInt(true));
    else if (chunks[1].startsWith("mapProgress")) {
      String argsWithPars = chunks[1].substring("mapProgress".length()).trim();
      String rawProgress = argsWithPars.substring(1,argsWithPars.length()-1).trim();
      float progress = Float.valueOf(rawProgress);
      return new ConditionSpec(chunks[0],new JobProgressInt(true,progress));
    }
    else if (chunks[1].startsWith("reduceProgress")) {
      String argsWithPars = chunks[1].substring("reduceProgress".length()).trim();
      String rawProgress = argsWithPars.substring(1,argsWithPars.length()-1).trim();
      float progress = Float.valueOf(rawProgress);
      return new ConditionSpec(chunks[0],new JobProgressInt(false,progress));
    }
    else
      return null;
  }

  public Condition make(Map<String,JobInProgress> jsidToJip) {
    if (!jsidToJip.containsKey(this.jobSpecID))
      throw new NullPointerException("Cannot find \"" + this.jobSpecID + "\" in "
            + jsidToJip);
    return this.condition.make(jsidToJip.get(this.jobSpecID));
  }

  @Override
  public String toString() {
    return this.jobSpecID + "." + this.condition.toString();
  }


  // Conditions

  private static interface ConditionInternal {
    public Condition make(JobInProgress jip);
  }

  private final static class JobInitedInt implements ConditionInternal {
    @Override
    public Condition make(JobInProgress jip) {
      return new JobInited(jip);
    }
    @Override
    public String toString() {
      return "inited";
    }
  }

  private final static class JobCompletedInt implements ConditionInternal {
    @Override
    public Condition make(JobInProgress jip) {
      return new JobCompleted(jip);
    }
    @Override
    public String toString() {
      return "completed";
    }
  }

  private final static class JobScheduleReducesInt implements ConditionInternal {
    @Override
    public Condition make(JobInProgress jip) {
      return new JobScheduleReduces(jip);
    }
    @Override
    public String toString() {
      return "scheduleReduces";
    }
  }

  private final static class JobProgressInt implements ConditionInternal {
    final boolean isMap;
    final float progress;
    public JobProgressInt(boolean isMap,float progress) {
      this.isMap = isMap;
      this.progress = progress;
    }
    @Override
    public Condition make(JobInProgress jip) {
      return new JobProgress(jip,this.isMap,this.progress);
    }
    @Override
    public String toString() {
      return (this.isMap ? "map" : "reduce") + "Progress("
            + Float.toString(this.progress) + ")";
    }
  }

  private final static class JobHasTaskSuspendedInt implements ConditionInternal {
    public final boolean isMap;
    public JobHasTaskSuspendedInt(boolean isMap) {
      this.isMap = isMap;
    }
    @Override
    public Condition make(JobInProgress jip) {
      return new JobHasTaskSuspended(jip,this.isMap);
    }
    @Override
    public String toString() {
      return "has" + (this.isMap ? "Map" : "Reduce") + "Suspended";
    }
  }
}
