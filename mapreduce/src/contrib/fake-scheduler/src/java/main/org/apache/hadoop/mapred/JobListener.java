//package org.apache.hadoop.mapred;
//
//
//public class JobListener extends JobInProgressListener {
//
//  private FakeScheduler scheduler;
//
//  public JobListener(FakeScheduler scheduler) {
//    this.scheduler = scheduler;
//  }
//
//  @Override
//  public void jobAdded(JobInProgress jip) {
//    this.scheduler.jobAdded(jip);
//    this.scheduler.jidToJip.put(jip.getJobID(), jip);
//  }
//
//  @Override
//  public void jobRemoved(JobInProgress jip) {
//  }
//
//  @Override
//  public void jobUpdated(JobChangeEvent event) {
//    if (event instanceof JobStatusChangeEvent) {
//      JobStatusChangeEvent statusEvent = (JobStatusChangeEvent) event;
//      if (statusEvent.getEventType() == JobStatusChangeEvent.EventType.RUN_STATE_CHANGED) {
//        JobStatus status = statusEvent.getNewStatus();
//        int runState = status.getRunState();
//        JobID jobID = JobID.downgrade(status.getJobID());
//        if (runState == JobStatus.FAILED) {
//          this.scheduler.jobFailed(jobID);
//        } else if (runState == JobStatus.KILLED) {
//          this.scheduler.jobKilled(jobID);
//        } else if (runState == JobStatus.SUCCEEDED) {
//          this.scheduler.jobSucceded(jobID);
//        }
//      }
//    }
//  }
//}
