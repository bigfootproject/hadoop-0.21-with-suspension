package org.apache.hadoop.mapred;

public class AssignReduceAction extends AssignTasksAction {

  public AssignReduceAction(JobInProgress jip) {
    this(jip, new JobReadyForReducerPrecondition(jip));
  }

  public AssignReduceAction(JobInProgress jip, PreconditionsChecker conds) {
    super(conds, new AssignTaskActionRunner(jip,false));
  }
}
