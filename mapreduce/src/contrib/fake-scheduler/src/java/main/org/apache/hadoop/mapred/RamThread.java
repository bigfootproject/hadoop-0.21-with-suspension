package org.apache.hadoop.mapred;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


public class RamThread extends Thread {

  private final static Log LOG = LogFactory.getLog(RamThread.class);
  private boolean stop;
  private File tmpFile;
  private PrintWriter tmpFileWriter;
  private final long time;

  public RamThread() throws IOException {
    this.time = System.currentTimeMillis();
    this.tmpFile = File.createTempFile(this.toString() + "_"
                                     + Long.toString(this.time) ,"txt");
    this.tmpFile.deleteOnExit();
    this.tmpFileWriter = new PrintWriter(new FileWriter(this.tmpFile));
    this.stop = false;
  }

  @Override
  public void run() {
    while (!this.stop) {
      try {
        
        Process process = new ProcessBuilder("/bin/ps","aux").start();
        InputStream is = process.getInputStream();
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        String line;
        StringBuilder builder = new StringBuilder();
        builder.append(System.currentTimeMillis()).append('\n');

        while ((line = br.readLine()) != null) {
          builder.append(line).append('\n');
        }

        this.tmpFileWriter.println(builder.toString());
        this.tmpFileWriter.println();

        Thread.sleep(1000);
      } catch (Exception e) {
        LOG.error(ExceptionUtils.getStackTrace(e));
      }
    }
  }

  public synchronized void terminate() {
    this.stop = true;
  }

  public synchronized void saveResult(PrintWriter writer)
      throws IOException {
    try {
      this.tmpFileWriter.close();
      BufferedReader reader = new BufferedReader(new FileReader(this.tmpFile));
      String line = null;
      while ((line = reader.readLine()) != null) {
        writer.println(line);
      }
      reader.close();
    } catch (FileNotFoundException e) {
      throw new IOException(e);
    }
  }
}
