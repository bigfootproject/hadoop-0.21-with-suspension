package org.apache.hadoop.mapred;

public class JobScheduleReduces implements Condition {
  
  private JobInProgress jip;

  public JobScheduleReduces(JobInProgress jip) {
    this.jip = jip;
  }

  public boolean check() {
    return this.jip.scheduleReduces();
  }

  @Override
  public String toString() {
    return this.jip.getJobID().toString() + ".scheduleReduces";
  }
}
