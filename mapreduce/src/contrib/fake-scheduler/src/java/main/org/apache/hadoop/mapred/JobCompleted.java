package org.apache.hadoop.mapred;


public class JobCompleted implements Condition {
  
  private JobInProgress jip;

  public JobCompleted(JobInProgress jip) {
    if (jip == null)
      throw new NullPointerException();
    this.jip = jip;
  }

  public boolean check() {
    return this.jip.getStatus().getRunState() == JobStatus.SUCCEEDED;
  }

  @Override
  public String toString() {
    return this.jip.getJobID().toString() + ".completed";
  }
}
