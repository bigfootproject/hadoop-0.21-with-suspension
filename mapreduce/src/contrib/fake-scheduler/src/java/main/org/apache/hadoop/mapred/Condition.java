package org.apache.hadoop.mapred;


public interface Condition {
  public boolean check();
}
