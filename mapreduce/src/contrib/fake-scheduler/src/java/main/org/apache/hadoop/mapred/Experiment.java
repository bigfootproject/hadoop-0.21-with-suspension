package org.apache.hadoop.mapred;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.HashSet;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.mapreduce.JobCounter;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapred.JobInProgress.JobSummary;
import org.apache.hadoop.mapreduce.server.jobtracker.TaskTracker;
import org.apache.hadoop.mapreduce.TaskType;


public class Experiment extends JobInProgressListener {

  private final static Log LOG = LogFactory.getLog(Experiment.class);

  /* Experiments specs */
  private Map<String,JobSpec> jobSpecs;
  private List<CondAndRunnerSpec> condAndRunnerSpecs;

  private List<CondAndRunner> condAndRunners;
  private Map<JobID,String> jidToJsid;
  private Map<String,JobInProgress> jsidToJip;
  private List<String> completed;
  private boolean allJobsSubmitted;
  public RunResultType lastResultType;
  public Set<String> ttHostnames;

  public final static String PS_DIR = "/tmp/FakeJob.ps.out";

  
  private Experiment(List<JobSpec> jobSpecs
                    ,List<CondAndRunnerSpec> condAndRunnerSpecs)
      throws IOException {
    this.jobSpecs = Collections.synchronizedMap(new HashMap<String,JobSpec>());
    for (JobSpec jobSpec : jobSpecs) {
      this.jobSpecs.put(jobSpec.getJobSpecID(), jobSpec);
    }
    this.lastResultType = RunResultType.SUCCEEDED;
    this.condAndRunnerSpecs = condAndRunnerSpecs;
    this.condAndRunners = new LinkedList<CondAndRunner>();
    this.jidToJsid = Collections.synchronizedMap(new HashMap<JobID,String>());
    this.jsidToJip = new HashMap<String,JobInProgress>();
    this.completed = new LinkedList<String>();
    this.allJobsSubmitted = false;
    this.ttHostnames = new HashSet<String>();
    LOG.info("created experiment: " + this.toString());
  }

  public JobID jsidToJid(String jobSpecID) {
    return this.jobSpecs.get(jobSpecID).getJobID();
  }

  public Collection<JobInProgress> getJobs(String queueName) {
    return this.jsidToJip.values();
  }

  private void createActions() {
    for (CondAndRunnerSpec spec : condAndRunnerSpecs) {
      LOG.info("  " + spec.toString());
      this.condAndRunners.add(spec.make(this.jsidToJip));
    }
  }

  public boolean hasNextAction() {
    return !this.condAndRunners.isEmpty();
  }

  public enum RunResultType {
      /** No more actions for this experiment */
      NO_MORE_ACTIONS
      /** Conditions for this actions are not met */
    , COND_FAILED
      /** Conditions are met, this actions has been executed */
    , SUCCEEDED
  };

  public static class RunResult {
    public final RunResultType type;
    public final List<Task> assigned;
    private RunResult(RunResultType type, List<Task> assigned) {
      this.type = type;
      this.assigned = assigned;
    }
    public static RunResult noMoreActions() {
      List<Task> empty = Collections.emptyList();
      return new RunResult( RunResultType.NO_MORE_ACTIONS, empty);
    }
    public static RunResult condFailed() {
      List<Task> empty = Collections.emptyList();
      return new RunResult( RunResultType.COND_FAILED, empty);
    }
    public static RunResult assignTasks(List<Task> tasks) {
      return new RunResult(RunResultType.SUCCEEDED, tasks);
    }
  }

  public RunResult runNextAction( JobTracker jobTracker
                                , TaskTracker taskTracker
                                , Collection<JobInProgress> jobs)
      throws IOException {
    String hostname = taskTracker.getStatus().getHost();
    if (!this.ttHostnames.contains(hostname))
      this.ttHostnames.add(hostname);

    if (!this.hasNextAction()) {
      if (this.lastResultType != RunResultType.NO_MORE_ACTIONS) {
        LOG.info("no more actions for this experiment");
        this.lastResultType = RunResultType.NO_MORE_ACTIONS;
      }
      return RunResult.noMoreActions();
    }

    for (JobInProgress jip : jobs) {
      LOG.info(jip.getJobID().toString() + ".progress{"
              + " map:"     + Float.toString(jip.getStatus().mapProgress())
              + ", reduce:" + Float.toString(jip.getStatus().reduceProgress()) + " }"
          );
    }

    CondAndRunner next = this.condAndRunners.get(0);

    if (!next.check()) {
      if (this.lastResultType != RunResultType.COND_FAILED) {
        LOG.info(next.cond.toString() + " failed");
        this.lastResultType = RunResultType.COND_FAILED;
      }
      return RunResult.condFailed();
    }

    this.condAndRunners.remove(0);
    this.lastResultType = RunResultType.SUCCEEDED;
    LOG.info(next.cond.toString() + " succeded, start running "
                                  + next.runner.toString());
    List<Task> assigned = next.run(jobTracker, taskTracker, jobs);
    LOG.info("finish running " + next.runner.toString() + " result "
            + assigned);

    return RunResult.assignTasks(assigned);
  }

  public boolean areAllJobsSubmitted() {
    return this.allJobsSubmitted;
  }


  @Override
  public void jobAdded(JobInProgress jip) {
    for (JobSpec jobSpec : this.jobSpecs.values()) {
      if (jobSpec.getJob() == null)
        continue; // job not yet submitted
      if (jobSpec.getJobID().equals(jip.getJobID())) {
        LOG.info("submitted " + jobSpec.getJobID() + " with jsid "
                              + jobSpec.getJobSpecID());
        jidToJsid.put(jobSpec.getJobID(),jobSpec.getJobSpecID());
      }
    }

    if (this.jidToJsid.get(jip.getJobID()) == null) {
      throw new NullPointerException("No Jsid for " + jip.getJobID());
    }

    LOG.info("added " + jip.getJobID().toString() + " with jsid "
                      + this.jidToJsid.get(jip.getJobID()));

    this.jsidToJip.put(this.jidToJsid.get(jip.getJobID()),jip);
    if (this.jobSpecs.size() == this.jsidToJip.size()) {
      LOG.info("all jobs have been submitted, creating actions:");
      this.createActions();
      this.allJobsSubmitted = true;
    }
  }

  @Override
  public void jobRemoved(JobInProgress jip) {
    LOG.info("job " + jip.getJobID().toString() + " removed");
  }

  @Override
  public void jobUpdated(JobChangeEvent event) {
    LOG.info("received event " + event.toString());
    if (event instanceof JobStatusChangeEvent) {
      JobStatusChangeEvent statusEvent = (JobStatusChangeEvent) event;
      if (statusEvent.getEventType() == JobStatusChangeEvent.EventType.RUN_STATE_CHANGED) {
        JobStatus status = statusEvent.getNewStatus();
        int runState = status.getRunState();
        JobID jobID = JobID.downgrade(status.getJobID());
        if (runState == JobStatus.FAILED) {
          this.jobFailed(jobID);
        } else if (runState == JobStatus.KILLED) {
          this.jobKilled(jobID);
        } else if (runState == JobStatus.SUCCEEDED) {
          this.jobSucceded(jobID);
        }
      }
    }
  }

  public void jobFailed(JobID jobID) {
    LOG.info("job " + jobID.toString() + " finished");
  }

  public void jobKilled(JobID jobID) {
    LOG.info("job " + jobID.toString() + " killed");
  }

  public void jobSucceded(JobID jobID) {
    LOG.info("job " + jobID.toString() + " succeded");
    this.completed.add(this.jidToJsid.get(jobID));
    if (this.finished()) {
      LOG.info("all jobs finished, experiment completed");
    }
  }

  public void saveResult(String file) throws IOException {
    LOG.info("start saving results into " + file);
    PrintWriter writer = new PrintWriter(new FileWriter(file));
    writer.println("Jobs:");
    for (JobSpec jobSpec : this.jobSpecs.values())
      writer.println("\t" + jobSpec.toString());
    writer.println();
    writer.println("Actions:");
    for (CondAndRunnerSpec actionSpec : this.condAndRunnerSpecs)
      writer.println("\t" + actionSpec.toString());
    writer.println();
    writer.println("JobSummaries:");
    for (JobInProgress jip : this.jsidToJip.values())
      writer.println(this.jobSummaryToString(jip));
    writer.println();
    for (String hostname : this.ttHostnames) {
      LOG.info("getting ps logs from " + hostname);
      String tmpDir = Integer.toString(Math.abs(("/tmp/" 
                                                + this.toString()
                                                + "_" + hostname).hashCode()));
      File d = new File(tmpDir);
      boolean success = d.mkdir();
      if (!success) {
        LOG.error("cannot create directory " + d.getAbsolutePath());
        continue;
      }
      try {
        ProcessBuilder ssh = new ProcessBuilder("scp"
                                               ,hostname + ":" + PS_DIR + "/*"
                                               ,tmpDir);
        LOG.info("copying ps files with " + ssh.command());
        final int exitValue = ssh.start().waitFor();
        if (exitValue != 0) {
          LOG.error("ssh returned exitValue " + Integer.toString(exitValue));
          continue;
        }
        for (File f : d.listFiles()) {
          writer.println("ps results for " + hostname + " file " + f.getName());
          BufferedReader r = new BufferedReader(new FileReader(f));
          String line = null;
          while((line = r.readLine()) != null)
            writer.println(line);
          r.close();
        }
      } catch (InterruptedException e) {
        LOG.error(e);
      }
      writer.println();
    }
    writer.println("ps results:");
    writer.close();
    LOG.info("finish saving results into " + file);
  }

  public boolean finished() {
    return this.allJobsSubmitted
        && (this.jobSpecs.size() == this.completed.size());
  }

  protected synchronized void submitJob(JobSpec jobSpec) {
    try {
      jobSpec.create();
      jobSpec.submit();
    } catch (Exception e) {
      LOG.error("cannot submit " + jobSpec.toString() + ": "
            + e.toString());
      LOG.error(ExceptionUtils.getStackTrace(e));
      
      throw new RuntimeException(ExceptionUtils.getStackTrace(e));
    }
  }

  public void submitJobs() {
    LOG.info("starting thread to submit jobs");
    new Thread() { public void run() {
      for (JobSpec jobSpec : jobSpecs.values())
        submitJob(jobSpec);
    } }.start();
  }

  /** Create a new Experiment
   *
   * format:
   * <jobId1> <file1> <numreduces1>
   * <jobId2> <file2> <numreduces2>
   * ...
   * <jobIdn> <filen> <numreducesn>
   *
   * when <cond1> do <runner1>
   * when <cond2> do <runner2>
   * ...
   * when <condm> do <runnerm>
   */
  public static Experiment fromStringOrNull(String str)
      throws IOException,InterruptedException,ClassNotFoundException {
    // split job definitions from conditions and runners
    String[] jobsActions = str.split("\\n{2,}");

    List<JobSpec> jobSpecs = new LinkedList<JobSpec>();
    LOG.info("start parsing job specs");
    for (String rawJob : jobsActions[0].split("\\n")) {
      JobSpec job = JobSpec.fromStringOrNull(rawJob.trim());
      if (job == null) {
        LOG.error("cannot parse \"" + rawJob + "\"");
        return null;
      }
      jobSpecs.add(job);
    }
    LOG.info("stop parsing job specs");

    List<CondAndRunnerSpec> condsAndRunners = new LinkedList<CondAndRunnerSpec>();
    LOG.info("start parsing actions");
    for (String rawCondsAndRunners : jobsActions[1].split("\\n")) {
      CondAndRunnerSpec condAndRunnerSpec = CondAndRunnerSpec.fromStringOrNull(
          rawCondsAndRunners.trim());
      if (condAndRunnerSpec == null) {
        LOG.error("cannot parse \"" + rawCondsAndRunners + "\"");
        return null;
      }
      condsAndRunners.add(condAndRunnerSpec);
    }
    LOG.info("stop parsing actions");

    return new Experiment(jobSpecs, condsAndRunners);
  }

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();

    for (JobSpec jobSpec : this.jobSpecs.values())
      builder.append(jobSpec.toString()).append('\n');

    builder.append('\n');

    for (CondAndRunnerSpec condAndRunnerSpec : this.condAndRunnerSpecs)
      builder.append(condAndRunnerSpec.toString()).append('\n');

    return builder.toString();
  }

  private String jobSummaryToString(JobInProgress job) {
    JobStatus status = job.getStatus();
    Counters jobCounters = job.getJobCounters();
    long mapSlotSeconds = 
      (jobCounters.getCounter(JobCounter.SLOTS_MILLIS_MAPS) +
       jobCounters.getCounter(JobCounter.FALLOW_SLOTS_MILLIS_MAPS)) / 1000;
    long reduceSlotSeconds = 
      (jobCounters.getCounter(JobCounter.SLOTS_MILLIS_REDUCES) +
       jobCounters.getCounter(JobCounter.FALLOW_SLOTS_MILLIS_REDUCES)) / 1000;

    return "jobId=" + job.getJobID() + "," +
           "submitTime=" + job.getStartTime() + "," +
           "launchTime=" + job.getLaunchTime() + "," +
           "mapFinishTime=" + JobSummary.getMapFinishTime(job) + "," +
           "reduceStartTime=" + JobSummary.getReduceStartTime(job) + "," +
           "finishTime=" + job.getFinishTime() + "," +
           "numMaps=" + job.getTasks(TaskType.MAP).length + "," +
           "numSlotsPerMap=" + job.getNumSlotsPerMap() + "," +
           "numReduces=" + job.getTasks(TaskType.REDUCE).length + "," +
           "numSlotsPerReduce=" + job.getNumSlotsPerReduce() + "," +
           "numSuspendedMaps=" + job.mapsSuspendedSinceStart + "," +
           "numResumedMaps=" + job.mapsResumedSinceStart + "," +
           "numSuspendedReduces=" + job.reducesSuspendedSinceStart + "," +
           "numResumedReduces=" + job.reducesResumedSinceStart + "," +
           "status=" + JobStatus.getJobRunState(status.getRunState()) + "," + 
           "mapSlotSeconds=" + mapSlotSeconds + "," +
           "reduceSlotsSeconds=" + reduceSlotSeconds  + ",";
  }
  

  static class CondAndRunnerSpec {
    ConditionSpec condSpec;
    RunnerSpec runSpec;
    private CondAndRunnerSpec(ConditionSpec condSpec, RunnerSpec runSpec) {
      this.condSpec = condSpec;
      this.runSpec = runSpec;
    }

    // format: when <condition> do <action>
    public static CondAndRunnerSpec fromStringOrNull(String rawStr) {
      String str = rawStr.trim();
      if (!str.startsWith("when ")) {
        LOG.error(str + " doesn't start with 'when '");
        return null;
      }
      int onIdx = str.indexOf(" do ");
      if (onIdx == -1) {
        LOG.error(str + " doesn't contain ' do '");
        return null;
      }
      
      // condition
      String rawCondition = str.substring("when ".length(),onIdx).trim();
      ConditionSpec condSpec = ConditionSpec.fromStringOrNull(rawCondition);
      if (condSpec == null) {
        LOG.error("condition \"" + rawCondition + "\" is not valid");
        return null;
      }

      String rawRunner = str.substring(onIdx + 4).trim();
      RunnerSpec runnerSpec = RunnerSpec.fromStringOrNull(rawRunner);
      if (runnerSpec == null) {
        LOG.error("runner \"" + rawRunner + "\" is not valid");
        return null;
      }

      return new CondAndRunnerSpec(condSpec, runnerSpec);
    }

    public CondAndRunner make(Map<String,JobInProgress> jsidToJip) {
      Condition cond = this.condSpec.make(jsidToJip);
      Runner runner = this.runSpec.make(jsidToJip);
      return new CondAndRunner(cond, runner);
    }

    @Override
    public String toString() {
      return "when " + this.condSpec.toString() + " do " + this.runSpec.toString();
    }
  }

  static class CondAndRunner {
    final Condition cond;
    final Runner runner;
    CondAndRunner(Condition cond,Runner runner) {
      this.cond = cond;
      this.runner = runner;
    }

    boolean check() {
      return this.cond.check();
    }

    List<Task> run(JobTracker jobTracker
                  , TaskTracker taskTracker
                  , Collection<JobInProgress> jobs) throws IOException {
      return this.runner.run(jobTracker, taskTracker, jobs);
    }
  }
}
