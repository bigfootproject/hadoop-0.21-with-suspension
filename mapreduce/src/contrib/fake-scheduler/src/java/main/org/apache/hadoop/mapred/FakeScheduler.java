package org.apache.hadoop.mapred;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.server.jobtracker.TaskTracker;

import org.apache.hadoop.mapred.Experiment.RunResult;
import org.apache.hadoop.mapred.Experiment.RunResultType;


/**
 * add file called "experimentfiles" into ${HADOOP_MAPRED_HOME}
 */
public class FakeScheduler extends TaskScheduler {

  private static final Log LOG = LogFactory.getLog(FakeScheduler.class);

  private Experiment current;
  private List<Experiment> experiments;
  private List<String> files;
  private List<Experiment> completed;
  private final long startTime;
  private boolean stop;

  /** Used to initialize jobs */
  private EagerTaskInitializationListener taskInitListener;

  public FakeScheduler() {
    if (!new java.io.File("experimentfiles").exists())
      throw new RuntimeException("File \"experimentfiles\" "
                               + "is missing in current directory "
                               + System.getProperty("user.dir"));

    this.startTime = System.currentTimeMillis();
    this.stop = false;
    LOG.info("FakeScheduler created with startTime: "
           + Long.toString(this.startTime));
  }

  private static String readFile(String file)
      throws IOException {
    StringBuilder builder = new StringBuilder();
    String tmp = null;
    BufferedReader reader = new BufferedReader(new FileReader(file));
    boolean first = true;
    while ((tmp = reader.readLine()) != null) {
      if (first) first = false;
      else builder.append('\n');
      builder.append(tmp);
    }
    reader.close();
    return builder.toString();
  }

  /** Initialize the internal state of the scheduler */
  public void init() throws IOException {
    LOG.info("started init");
    this.current = null;
    this.experiments = new LinkedList<Experiment>();
    this.files = new LinkedList<String>();
    this.completed = new LinkedList<Experiment>();

    // add experiments
    for (String file : readFile("experimentfiles").split("\n")) {
      String rawExperiment = readFile(file);
      try {
        Experiment experiment = Experiment.fromStringOrNull(rawExperiment);
        if (experiment == null) {
          LOG.error("cannot parse file " + file + " with content \n"
                  + rawExperiment + "\n");
        } else {
          this.experiments.add(experiment);
          this.files.add(file);
        }
      } catch (Exception e) {
        throw new IOException(e);
      }
    }

    this.taskInitListener = new EagerTaskInitializationListener(this.getConf());
    this.taskInitListener.setTaskTrackerManager(this.taskTrackerManager);
    this.taskInitListener.start();
    this.taskTrackerManager.addJobInProgressListener(this.taskInitListener);
    
    LOG.info("finished init");
  }

  public void runNextExperiment() {
    if (this.experiments.isEmpty()) {
      LOG.info("no more experiments to run, stopping the scheduler");
      this.stop = true;
      return;
    }
    if (this.current != null) {
      this.taskTrackerManager.removeJobInProgressListener(this.current);
    }
    this.current = this.experiments.remove(0);
    this.taskTrackerManager.addJobInProgressListener(this.current);
    this.current.submitJobs();
  }


  @Override
  public synchronized void start() throws IOException {
    LOG.info("started start()");
    this.init();
    this.runNextExperiment();
    LOG.info("finished start()");
  }

  @Override
  public synchronized void terminate() throws IOException {
    if (this.current != null)
      this.taskTrackerManager.removeJobInProgressListener(this.current);
    //this.listener.terminate();
    if (this.taskInitListener != null) {
      this.taskTrackerManager.removeJobInProgressListener(this.taskInitListener);
      this.taskInitListener.terminate();
    }
  }

  private void doAbort() throws IOException {
    ((JobTracker) this.taskTrackerManager).stopTracker();
    System.exit(1);
  }

  private void experimentFinished() throws IOException {
    LOG.info("current experiment completed");
    this.current.saveResult(this.files.remove(0) + ".out");
    this.completed.add(this.current);
    this.runNextExperiment();
  }

  @Override
  public List<Task> assignTasks(TaskTracker taskTracker) throws IOException {
    if (this.stop || this.current == null)
      return Collections.emptyList();

    if (!this.current.areAllJobsSubmitted()) {
      LOG.info("waiting for jobs to be submitted");
      return Collections.emptyList();
    }

    if (this.current.finished()) {
      this.experimentFinished();
      return Collections.emptyList();
    }

    RunResult result = this.current.runNextAction(
          (JobTracker) this.taskTrackerManager
        , taskTracker
        , this.getJobs(""));

//    if (!result.assigned.isEmpty())
//      LOG.info("next action result " + result.assigned);

    // check the type of result

    return result.assigned;
  }

  @Override
  public Collection<JobInProgress> getJobs(String queueName) {
    if (this.current == null)
      return Collections.emptyList();
    else
      return this.current.getJobs(queueName);
  }
}
