package org.apache.hadoop.mapred;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.IOException;
import java.lang.InterruptedException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;


public class FakeJob extends Job {

  public final static Log LOG = LogFactory.getLog(FakeJob.class);

  public FakeJob(String input, int numReducers, int bytesOccupiedByMappers
                ,String psFile)
      throws IOException, InterruptedException {
    super();

    LOG.info("started creating new job");

    Configuration conf = this.getConfiguration();
    conf.setInt("bytesOccupiedByMappers",bytesOccupiedByMappers);
    conf.set("psFile", psFile);
  
    this.setInputFormatClass(TextInputFormat.class);
    this.setMapperClass(FakeMapper.class);
    this.setMapOutputKeyClass(LongWritable.class);
    this.setMapOutputValueClass(Text.class);
    this.setReducerClass(Reducer.class);
    this.setOutputKeyClass(LongWritable.class);
    this.setOutputValueClass(Text.class);
    this.setOutputFormatClass(TextOutputFormat.class);
    TextInputFormat.addInputPath(this, new Path(input));
    TextOutputFormat.setOutputPath(this, new Path("FakeOutput_" 
                                  + Long.toString(System.currentTimeMillis())));
    this.setNumReduceTasks(numReducers);
    this.setJarByClass(FakeJob.class);

    LOG.info("finished creating new job");
  }

}

class FakeMapper extends Mapper<LongWritable,Text,LongWritable,Text> {
  private byte[] garbage;
  private RamThread ramThread;

  @Override
  public void setup(Context context) throws IOException {
    this.ramThread = new RamThread();
    this.ramThread.start();
    int bs = context.getConfiguration().getInt("bytesOccupiedByMappers",0);
    this.garbage = new byte[bs];
  }

  @Override
  public void map(LongWritable offset,Text line,Context context) 
      throws IOException,InterruptedException {
    super.map(offset,line,context);
    System.out.println(this.garbage);
  }

  @Override
  public void cleanup(Context context) throws IOException {
    System.out.println(this.garbage);
    this.ramThread.stop();
    String psFile = context.getConfiguration().get("psFile","/tmp/FakeJob.ps.out");
    File dir = new File(psFile);
    if (!dir.isDirectory())
      dir.mkdir();
    File tempFile = File.createTempFile(this.toString(), "ps.out", dir);
    PrintWriter p = new PrintWriter(new FileWriter(tempFile));
    this.ramThread.saveResult(p);
    p.close();
  }
}
