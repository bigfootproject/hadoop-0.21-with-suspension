package org.apache.hadoop.mapred;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

import org.apache.hadoop.mapreduce.server.jobtracker.TaskTracker;


public interface Runner {
   public List<Task> run(JobTracker jobTracker
                       , TaskTracker taskTracker
                       , Collection<JobInProgress> jobs) throws IOException;
}
