package org.apache.hadoop.mapred;

public class JobInited implements Condition {
  
  private JobInProgress jip;

  public JobInited(JobInProgress jip) {
    if (jip == null)
      throw new NullPointerException();
    this.jip = jip;
  }

  public boolean check() {
    return this.jip.inited();
  }

  @Override
  public String toString() {
    return this.jip.getJobID().toString() + ".inited";
  }
}
