package org.apache.hadoop.mapred;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.hadoop.mapreduce.server.jobtracker.TaskTracker;


public class AssignTask implements Runner {
  
  private final boolean isMap;
  private final JobInProgress jip;
  private Task assigned;

  public AssignTask(JobInProgress jip,boolean isMap) {
    this.jip = jip;
    this.isMap = isMap;
    this.assigned = null;
  }

  @Override
  public List<Task> run(JobTracker jobTracker
                       , TaskTracker taskTracker
                       , Collection<JobInProgress> jobs) throws IOException {
    this.assigned = this.obtainNewTask( this.jip
                                      , this.isMap
                                      , jobTracker
                                      , taskTracker.getStatus());
    if (this.assigned == null)
      return Collections.emptyList();
    else
      return Collections.singletonList(this.assigned);
  }

  private Task obtainNewTask( JobInProgress jip
                            , boolean isMap
                            , JobTracker jobTracker
                            , TaskTrackerStatus status) throws IOException {
    int numUniqueHosts = jobTracker.getNumberOfUniqueHosts();
    ClusterStatus clusterStatus = jobTracker.getClusterStatus();

    return isMap ? jip.obtainNewMapTask( status
                                       , clusterStatus.getMaxMapTasks()
                                       , numUniqueHosts
                                       , jip.anyCacheLevel)
                 : jip.obtainNewReduceTask( status
                                          , clusterStatus.getMaxReduceTasks()
                                          , numUniqueHosts);
  }

  @Override
  public String toString() {
    return "assign" + (isMap ? "Map" : "Reduce") 
                    + "(" + jip.getJobID().toString() + ")";
  }
}
