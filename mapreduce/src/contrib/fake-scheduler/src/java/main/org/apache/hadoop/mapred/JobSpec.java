package org.apache.hadoop.mapred;

import java.io.IOException;

import org.apache.hadoop.mapreduce.Job;


public class JobSpec {

  private final String jobSpecID;
  private Job job;
  private final String input;
  private final int numReducers;
  private final int bytesOccupiedByMappers;
  private boolean submitted;

  private JobSpec(String jobSpecID
                 ,String input) throws IOException,InterruptedException {
    this(jobSpecID,input,1,0);
  }

  private JobSpec(String jobSpecID, String input, int numReducers
                 ,int bytesOccupiedByMappers) 
      throws IOException,InterruptedException {
    this.jobSpecID = jobSpecID;
    this.input = input;
    this.numReducers = numReducers;
    this.bytesOccupiedByMappers = bytesOccupiedByMappers;
    this.job = null;
    this.submitted = false;
  }

  public JobID getJobID() {
    return JobID.downgrade(this.job.getJobID());
  }

  public String getJobSpecID() {
    return this.jobSpecID;
  }

  public Job getJob() {
    return this.job;
  }

  public boolean isSubmitted() {
    return this.submitted;
  }

  public void create()
      throws IOException,InterruptedException,ClassNotFoundException {
    if (this.job != null)
      return;

    this.job = new FakeJob(this.input
                          ,this.numReducers
                          ,this.bytesOccupiedByMappers
                          ,Experiment.PS_DIR);
  }

  public void submit()
      throws IOException,InterruptedException,ClassNotFoundException {
    if (this.submitted)
      return;

    this.job.submit();
    this.submitted = true;
  }

  // parse string in the format:
  // <jobSpecID> = <input_file> <num_reduces>
  // can return null (change with Result)
  public static JobSpec fromStringOrNull(String str)
      throws IOException,InterruptedException {
    String[] equalSplit = str.split("=");
    if (equalSplit.length != 2)
      return null;

    String jobSpecID = equalSplit[0].trim();

    String[] splitted = equalSplit[1].trim().split("\\s+");
    if (splitted.length < 1 || splitted.length > 3)
      return null;

    String input = splitted[0].trim();
    if (splitted.length == 1)
      return new JobSpec(jobSpecID, input);
    else {
      return new JobSpec(jobSpecID,input,Integer.valueOf(splitted[1].trim())
                                        ,Integer.valueOf(splitted[2].trim()));
    }
  }

  @Override
  public String toString() {
    return this.jobSpecID + " = " + this.input
                          + " " + Integer.toString(this.numReducers);
  }
}
