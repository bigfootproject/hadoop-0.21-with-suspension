package org.apache.hadoop.mapred;


public class JobProgress implements Condition {
  private final JobInProgress jip;
  private final boolean isMap;
  private final float progress;

  public JobProgress(JobInProgress jip, boolean isMap, float progress) {
    this.jip = jip;
    this.isMap = isMap;
    this.progress = progress;
  }

  @Override
  public boolean check() {
    return (this.isMap ? this.jip.getStatus().mapProgress()
                       : this.jip.getStatus().reduceProgress()) >= this.progress;
  }
}
