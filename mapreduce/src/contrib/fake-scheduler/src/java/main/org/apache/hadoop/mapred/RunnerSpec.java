package org.apache.hadoop.mapred;

import java.util.Map;


public class RunnerSpec {

  private final String jobSpecID;
  private final RunnerInt runnerInt;

  private RunnerSpec(String jobSpecID, RunnerInt runnerInt) {
    this.jobSpecID = jobSpecID;
    this.runnerInt = runnerInt;
  }

  // a runner is written <jobid>.<action>
  public static RunnerSpec fromStringOrNull(String str) {
    String[] chunks = str.split("\\.");
    if (chunks.length != 2)
      return null;

    if ("assignMap".equals(chunks[1]))
      return new RunnerSpec(chunks[0], new AssignTaskInt(true));
    else if ("assignReduce".equals(chunks[1]))
      return new RunnerSpec(chunks[0], new AssignTaskInt(false));
    else if ("suspendMap".equals(chunks[1]))
      return new RunnerSpec(chunks[0], new SuspendTaskInt(true));
    else if ("resumeMap".equals(chunks[1]))
      return new RunnerSpec(chunks[0], new ResumeTaskInt(true));
    else if ("killMap".equals(chunks[1]))
      return new RunnerSpec(chunks[0], new KillTaskInt(true));
    else
      return null;
  }

  public Runner make(Map<String,JobInProgress> jsidToJip) {
    return this.runnerInt.make(jsidToJip.get(this.jobSpecID));
  }

  @Override
  public String toString() {
    return this.jobSpecID + "." + this.runnerInt.toString();
  }

  private static interface RunnerInt {
    public Runner make(JobInProgress jip);
  }

  private static class AssignTaskInt implements RunnerInt {
    private boolean isMap;
    public AssignTaskInt(boolean isMap) {
      this.isMap = isMap;
    }
    @Override
    public Runner make(JobInProgress jip) {
      return new AssignTask(jip,this.isMap);
    }
    @Override
    public String toString() {
      return "assign" + (this.isMap ? "Map" : "Reduce");
    }
  }

  private static class SuspendTaskInt implements RunnerInt {
    private boolean isMap;
    public SuspendTaskInt(boolean isMap) {
      this.isMap = isMap;
    }
    @Override
    public Runner make(JobInProgress jip) {
      return new SuspendTask(jip,this.isMap);
    }
    @Override
    public String toString() {
      return "suspend" + (this.isMap ? "Map" : "Reduce");
    }
  }

  private static class ResumeTaskInt implements RunnerInt {
    private final boolean isMap;
    public ResumeTaskInt(boolean isMap) {
      this.isMap = isMap;
    }
    @Override
    public Runner make(JobInProgress jip) {
      return new ResumeTask(jip,this.isMap);
    }
    @Override
    public String toString() {
      return "reduce" + (this.isMap ? "Map" : "Reduce");
    }
  }

  private static class KillTaskInt implements RunnerInt {
    private final boolean isMap;
    public KillTaskInt(boolean isMap) {
      this.isMap = isMap;
    }
    @Override
    public Runner make(JobInProgress jip) {
      return new KillTask(jip,this.isMap);
    }
    @Override
    public String toString() {
      return "kill" + (this.isMap ? "Map" : "Reduce");
    }
  }
}
