//package org.apache.hadoop.mapred;
//
//import java.io.StringWriter;
//import java.io.PrintWriter;
//import java.io.IOException;
//
//import org.apache.commons.logging.Log;
//import org.apache.commons.logging.LogFactory;
//
//
//public class JobSubmitterThread extends Thread {
//
//  private final static Log LOG = LogFactory.getLog(JobSubmitterThread.class);
//
//  private final FakeScheduler scheduler;
//
//  public JobSubmitterThread(FakeScheduler scheduler) {
//    super();
//    this.scheduler = scheduler;
//  }
//
//  @Override
//  public void run() {
//    LOG.info("started submitting jobs thread");
//    try {
//      LOG.info("creating new job");
//      FakeJob job = new FakeJob();
//      LOG.info("created job " + job.getJobID());
//      this.scheduler.submitJob(job);
//    } catch(Exception e) {
//      StringWriter sw = new StringWriter();
//      e.printStackTrace(new PrintWriter(sw));
//      String stackTrace = sw.toString();
//      LOG.error("Error: " + e.toString() + " " + stackTrace);
//      this.scheduler.abort = true;
//      return;
//    }
//    LOG.info("finished submitting jobs thread");
//  }
//}
