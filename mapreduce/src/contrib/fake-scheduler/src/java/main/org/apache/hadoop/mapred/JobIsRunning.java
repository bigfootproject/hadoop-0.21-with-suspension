package org.apache.hadoop.mapred;

import java.util.Collection;

import org.apache.hadoop.mapreduce.server.jobtracker.TaskTracker;


public class JobIsRunning extends PreconditionsChecker {

  protected final JobInProgress job;
  protected final boolean isMap;
  protected final float progress;

  public JobIsRunning(JobInProgress job, boolean isMap, float progress) {
    this.job = job;
    this.isMap = isMap;
    this.progress = progress; 
  }

  @Override
  public boolean check( JobTracker jobTracker
                      , TaskTracker taskTracker
                      , Collection<JobInProgress> jobs) {
    
    float currentProgress = this.isMap ? this.job.status.mapProgress()
                                       : this.job.status.reduceProgress();

    return currentProgress >= progress;
  }

}
