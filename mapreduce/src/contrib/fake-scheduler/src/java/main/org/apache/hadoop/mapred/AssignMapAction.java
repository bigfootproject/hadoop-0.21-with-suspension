package org.apache.hadoop.mapred;

public class AssignMapAction extends AssignTasksAction {

  public AssignMapAction(JobInProgress jip) {
    this(jip, new JobReadyPrecondition(jip));
  }

  public AssignMapAction(JobInProgress jip, PreconditionsChecker conds) {
    super(conds, new AssignTaskActionRunner(jip,true));
  }
}
