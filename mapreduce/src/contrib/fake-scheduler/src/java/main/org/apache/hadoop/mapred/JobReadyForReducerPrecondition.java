package org.apache.hadoop.mapred;

import java.util.Collection;

import org.apache.hadoop.mapreduce.server.jobtracker.TaskTracker;


public class JobReadyForReducerPrecondition extends PreconditionsChecker {

  private final JobInProgress jip;

  public JobReadyForReducerPrecondition(JobInProgress jip) {
    this.jip = jip;
  }

  @Override
  public boolean check( JobTracker jobTracker
                      , TaskTracker taskTracker
                      , Collection<JobInProgress> jobs) {
    return this.jip.scheduleReduces();
  }

  @Override
  public String toString() {
    return this.jip.getJobID().toString() + ".scheduleReduces()";
  }

}
