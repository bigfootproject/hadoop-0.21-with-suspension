package org.apache.hadoop.mapred;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.hadoop.mapred.TaskStatus.State;
import org.apache.hadoop.mapreduce.TaskType;
import org.apache.hadoop.mapreduce.server.jobtracker.TaskTracker;


public class ResumeTask implements Runner {

  private final JobInProgress jip;
  private final TaskType taskType;

  public ResumeTask(JobInProgress jip,boolean isMap) {
    this.jip = jip;
    this.taskType = (isMap ? TaskType.MAP : TaskType.REDUCE);
  }

  public List<Task> run(JobTracker jobTracker
                      , TaskTracker taskTracker
                      , Collection<JobInProgress> jobs) throws IOException {
    List<TaskStatus> taskStatuses = taskTracker.getStatus().getTaskReports();
    TaskStatus selected = null;
    for (TaskStatus taskStatus : taskStatuses) {
      if (taskStatus.getTaskID().getTaskType() == this.taskType
       && taskStatus.getTaskID().getJobID().equals(jip.getJobID())
       && taskStatus.getRunState() == State.SUSPENDED) {
        selected = taskStatus;
        break;
       }
    }
    
    // TODO: instead you should return error
    if (selected != null) {
      try {
        jobTracker.resumeTask(selected.getTaskID());
      } catch (InterruptedException e) {
        throw new IOException(e);
      }
    }

    return Collections.emptyList();
  }

  @Override
  public String toString() {
    return this.jip.getJobID().toString() + ".resume"
        + (this.taskType == TaskType.MAP ? "Map" : "Reduce"); 
  }
}
