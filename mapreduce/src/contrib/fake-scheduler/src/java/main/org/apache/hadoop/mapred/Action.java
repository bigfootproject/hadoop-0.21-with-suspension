package org.apache.hadoop.mapred;

import java.io.IOException;
import java.util.Collection;

import org.apache.hadoop.mapreduce.server.jobtracker.TaskTracker;


public class Action {

  protected PreconditionsChecker checker;
  protected ActionRunner runner;

  public Action(PreconditionsChecker checker, ActionRunner runner) {
    this.checker = checker;
    this.runner = runner;
  }

  public boolean checkAndRun( JobTracker jobTracker
                            , TaskTracker taskTracker
                            , Collection<JobInProgress> jobs)
      throws IOException {
    if (!this.checker.check(jobTracker, taskTracker, jobs))
      return false; // action not ready to be executed
    else {
      this.runner.run(jobTracker, taskTracker,jobs);
      return true;
    }
  }

  @Override
  public String toString() {
    return "(precond:" + this.checker.toString()
        + ", action:"  + this.runner.toString() + ")";
  }
}
