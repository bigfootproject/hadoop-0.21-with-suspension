package org.apache.hadoop.mapred;


public class JobHasTaskSuspended implements Condition {

  private final JobInProgress jip;
  private final boolean isMap;

  // TODO: add kind of task
  public JobHasTaskSuspended(JobInProgress jip,boolean isMap) {
    this.jip = jip;
    this.isMap = isMap;
  }

  @Override
  public boolean check() {
    return 0 < (this.isMap ? this.jip.getSuspendedMapTasks()
                           : this.jip.getSuspendedReduceTasks());
  }

  @Override
  public String toString() {
    return this.jip.getJobID() + ".has" + (this.isMap ? "Maps" : "Reduces" )
                               + "Suspended";
  }
}
